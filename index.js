'use strict';

const electron = require('electron');  
const app = electron.app;  
const BrowserWindow = electron.BrowserWindow;

let mainWindow;
var Este = "";

app.on('window-all-closed', function() {  
	if(process.platform != 'darwin') {
	app.quit();
	}
});

app.on('ready', function() { 
	mainWindow = new BrowserWindow({width: 800, height: 600});
	console.log(__dirname);
	mainWindow.loadURL('file://' + __dirname + '/app/index.html');

	mainWindow.webContents.openDevTools();

	mainWindow.on('closed', function() {
	mainWindow = null;
	});
});